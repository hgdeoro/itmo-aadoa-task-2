import abc
import logging
import random
from dataclasses import dataclass

import matplotlib.pyplot as plt
import numpy as np
import sys
from matplotlib.axes import Axes
from matplotlib.figure import Figure
from scipy import optimize
from scipy.optimize import minimize_scalar, OptimizeResult

WORKERS = 1
X = 0
Y = 1


@dataclass
class DataWrapper:
    seed: int = 2020  # default seed value for random class
    data = None

    def __post_init__(self):
        self.random = random.Random(self.seed)
        self.alpha = self.random.random()
        self.beta = self.random.random()

    def prepare_data(self) -> 'DataWrapper':
        """
        Prepares the data for the algorithms
        """
        assert self.data is None
        data = np.zeros((101, 2))
        for k in range(0, 100 + 1):
            delta = self.random.normalvariate(mu=0, sigma=1)
            x = float(k) / 100.0
            y = self.alpha * x + self.beta + delta

            data[k][X] = x
            data[k][Y] = y

        self.data = data
        return self


class PartTwo(abc.ABC):
    def __init__(self, data_wrapper: DataWrapper):
        self.eval_count = 0
        self.data_wrapper: DataWrapper = data_wrapper

    @abc.abstractmethod
    def approximant_function(self, x, a, b):
        raise NotImplementedError()

    # def write_result(self, output_file: pathlib.Path, a, b):
    #     result = f"{a},{b},{self.eval_count}\n"
    #     pathlib.Path('data').joinpath(output_file).write_text(result)

    def means_of_least_squares(self, ab):
        """
        Function to optimize. Calculates means of least squares
        """
        a, b = ab
        assert self.data_wrapper is not None
        result = 0.0
        for k in range(0, 100 + 1):
            x = self.data_wrapper.data[k][0]
            y = self.data_wrapper.data[k][1]
            evaluated_value = self.approximant_function(x, a, b)
            result += (evaluated_value - y) ** 2

        logging.debug("a, b -> %s = %s", ab, result)
        self.eval_count += 1
        return result

    def plot(self, brute_force, gauss, nelder_mead, output_plot_path):
        x = self.data_wrapper.data[:, 0]
        y_noisy = self.data_wrapper.data[:, 1]

        fig, ax = plt.subplots()
        fig: Figure
        ax: Axes

        y_clean = [
            self.data_wrapper.alpha * i + self.data_wrapper.beta
            for i in x
        ]

        y_brute_force = [
            self.approximant_function(i, brute_force['a'], brute_force['b'])
            for i in x
        ]

        y_gauss = [
            self.approximant_function(i, gauss['a'], gauss['b'])
            for i in x
        ]

        y_nelder_mead = [
            self.approximant_function(i, nelder_mead['a'], nelder_mead['b'])
            for i in x
        ]

        ax.plot(x, y_noisy, '.', alpha=0.5, label="$y = \\alpha x + \\beta + \\delta$")
        ax.plot(x, y_clean, '-', alpha=0.2, label="$y = \\alpha x + \\beta$")
        ax.plot(x, y_brute_force, '-', alpha=0.5, label="Brute force")
        ax.plot(x, y_gauss, '-', alpha=0.5, label="Gauss")
        ax.plot(x, y_nelder_mead, '-', alpha=0.5, label="Nelder Mead")

        ax.set_xlabel('x')
        ax.set_ylabel('y')
        ax.legend()

        fig.savefig(str(output_plot_path))

    # --------------------------------------------------
    # Brute force
    # --------------------------------------------------

    def brute_approximation(self, rranges):
        """
        Implementation of brute force approximation
        """
        self.eval_count = 0
        # We need to set finish=None to make sure `optimize.brute` does not any
        # further optimization after brute force.
        logging.info("Starting brute force - rranges=%s", rranges)
        res_brute = optimize.brute(
            self.means_of_least_squares,
            rranges,
            full_output=False,
            finish=None,
            workers=WORKERS,
        )
        return dict(a=res_brute[0], b=res_brute[1], eval_count=self.eval_count, iter_count=-1)

    # --------------------------------------------------
    # Gauss
    # --------------------------------------------------

    def _gauss_approximant_function_by_a(self, a, args):
        params = args[0]
        b = params['b']
        return self.means_of_least_squares([a, b])

    def _gauss_approximant_function_by_b(self, b, args):
        params = args[0]
        a = params['a']
        return self.means_of_least_squares([a, b])

    def gauss_approximation(self, epsilon, x0, max_iter=10000):
        self.eval_count = 0
        logging.debug("Starting gauss_approximation(): %s", [epsilon, x0])
        assert epsilon > 0

        a, b = x0[0], x0[1]
        prev_funcion_value = None
        iter_count = 0
        result_found = False

        while iter_count < max_iter:
            iter_count += 1
            prev_a = a
            prev_b = b

            result = minimize_scalar(self._gauss_approximant_function_by_a, args=[dict(b=b)])
            logging.debug("a: %s -> %s", a, result.x)
            a = result.x

            result = minimize_scalar(self._gauss_approximant_function_by_b, args=[dict(a=a)])
            logging.debug("b: %s -> %s", b, result.x)
            b = result.x

            # Check stop conditions
            if abs(a - prev_a) < epsilon and abs(b - prev_b) < epsilon:
                result_found = True
                logging.debug("Result found: "
                              "abs(a - prev_a) < epsilon and "
                              "abs(b - prev_b) < epsilon")
                break

            if prev_funcion_value is None:  # in first iteration will be 'None'
                prev_funcion_value = self.means_of_least_squares([prev_a, prev_b])

            current_funcion_value = self.means_of_least_squares([a, b])

            if abs(current_funcion_value - prev_funcion_value) < epsilon:
                result_found = True
                logging.debug(
                    "Result found: "
                    "abs(current_funcion_value - prev_funcion_value) < epsilon"
                )
                break

            prev_funcion_value = current_funcion_value

        assert result_found

        return dict(a=a, b=b, eval_count=self.eval_count, iter_count=iter_count)

    # --------------------------------------------------
    # Nelder-Mead
    # --------------------------------------------------

    def nelder_mead_approximation(self, epsilon):
        """
        Uses scypi implementation of Nelder-Mead
        """
        self.eval_count = 0
        initial_guess = [1, 1]
        options = dict(
            # Absolute error in x between iterations
            # that is acceptable for convergence
            xatol=epsilon,
        )
        res: OptimizeResult = optimize.minimize(
            self.means_of_least_squares,
            initial_guess,
            method="Nelder-Mead",
            options=options,
        )
        assert res.success, f"nelder_mead_approximation() was not successfull"
        return dict(a=res.x[0], b=res.x[1], eval_count=self.eval_count, iter_count=res.nit)  # FIXME: iter_count


class PartTwoLinear(PartTwo):
    def approximant_function(self, x, a, b):
        return a * x + b


class PartTwoRational(PartTwo):
    def approximant_function(self, x, a, b):
        return a / 1 + b * x


def main(epsilon=None, seed=None):
    logging.info("seed=%s", seed)
    data_wrapper = DataWrapper(seed=seed).prepare_data()
    linear = PartTwoLinear(data_wrapper)
    rational = PartTwoRational(data_wrapper)

    logging.info("alpha=%s", data_wrapper.alpha)
    logging.info("beta=%s", data_wrapper.beta)

    EPSILON = epsilon or 0.001

    # ============================================================
    # Gauss
    # ============================================================

    # Linear
    result_gauss_linear = linear.gauss_approximation(EPSILON, [1.0, 1.0])
    logging.info("result_gauss_linear: %s", result_gauss_linear)

    # Rational
    result_gauss_rational = rational.gauss_approximation(EPSILON, [1.0, 1.0])
    logging.info("result_gauss_rational: %s", result_gauss_rational)

    # ============================================================
    # Nelder-Mead
    # ============================================================

    # Linear
    result_nelder_mead_linear = linear.nelder_mead_approximation(epsilon=EPSILON)
    logging.info("result_nelder_mead_linear: %s", result_nelder_mead_linear)

    # Rational
    result_nelder_mead_rational = rational.nelder_mead_approximation(epsilon=EPSILON)
    logging.info("result_nelder_mead_rational: %s", result_nelder_mead_rational)

    # ============================================================
    # Brute Force
    # ============================================================

    brute_force_rranges = (slice(0.0, 1.0 + EPSILON, EPSILON),
                           slice(0.0, 1.0 + EPSILON, EPSILON))

    # Linear
    result_bf_linear = linear.brute_approximation(rranges=brute_force_rranges)
    logging.info("result_bf_linear: %s", result_bf_linear)

    # Rational
    result_bf_rational = rational.brute_approximation(rranges=brute_force_rranges)
    logging.info("result_bf_rational: %s", result_bf_rational)

    # ============================================================
    # Plot
    # ============================================================

    linear.plot(
        result_bf_linear,
        result_gauss_linear,
        result_nelder_mead_linear,
        'data/multi-dim-linear.pdf'
    )
    rational.plot(
        result_bf_rational,
        result_gauss_rational,
        result_nelder_mead_rational,
        'data/multi-dim-rational.pdf'
    )


def setup_logging(level=logging.DEBUG):
    logging.basicConfig(level=level, stream=sys.stdout)
    logging.getLogger('matplotlib').setLevel(logging.WARNING)


if __name__ == '__main__':
    setup_logging()
    main(seed=6832299506)
    logging.info("Finished OK")
