import logging
import math
import sys
import typing
from dataclasses import dataclass

import numpy as np


#
# Implementation of optimization algorithms
#

@dataclass
class OneDimExhaustiveSearch:
    a: float
    b: float
    epsilon: float
    function_callable: typing.Callable

    def __post_init__(self):
        assert self.a < self.b
        self.eval_count = 0
        self.result = None

    def f(self, x):
        self.eval_count += 1
        return self.function_callable(x)

    def optimize(self):
        assert self.result is None
        n = math.ceil((self.b - self.a) / self.epsilon)
        xk = np.linspace(self.a, self.b, n + 1)
        values = [(x, self.f(x)) for x in xk]

        values.sort(key=lambda k: k[1])
        self.result = values[0][0]


@dataclass
class OneDimDichotomy:
    a: float
    b: float
    epsilon: float
    delta: float
    function_callable: typing.Callable

    def __post_init__(self):
        assert self.a < self.b
        assert 0 < self.delta < self.epsilon
        self.eval_count = 0
        self.result = None
        self.iter_count = 0

    def f(self, x):
        self.eval_count += 1
        return self.function_callable(x)

    def optimize(self):
        assert self.result is None
        a0 = self.a
        b0 = self.b

        x1 = x2 = f1 = f2 = None

        while abs(a0 - b0) >= self.epsilon:
            self.iter_count += 1

            x1 = (a0 + b0 - self.delta) / 2
            x2 = (a0 + b0 + self.delta) / 2

            f1 = self.f(x1)
            f2 = self.f(x2)

            if f1 <= f2:
                a0 = a0
                b0 = x2
            else:
                a0 = x1
                b0 = b0

        assert x1 is not None
        assert x2 is not None

        if f1 <= f2:
            self.result = x1
        else:
            self.result = x2


@dataclass
class OneDimGoldenSection:
    a: float
    b: float
    epsilon: float
    function_callable: typing.Callable

    def __post_init__(self):
        assert self.a < self.b
        self.eval_count = 0
        self.result = None
        self.iter_count = 0

    def f(self, x):
        self.eval_count += 1
        return self.function_callable(x)

    def optimize(self):
        assert self.result is None

        a0 = self.a
        b0 = self.b

        x1 = x2 = f1 = f2 = None

        first_iteration = True
        while abs(a0 - b0) >= self.epsilon:
            self.iter_count += 1

            x1 = a0 + ((3.0 - math.sqrt(5)) / 2) * (b0 - a0)
            x2 = b0 + ((math.sqrt(5) - 3.0) / 2) * (b0 - a0)

            assert abs((x1 + x2) / 2) - ((a0 + b0) / 2) <= 1e-16

            if first_iteration:
                first_iteration = True
            else:
                assert (x1 is None and f1 is None) or (x2 is None and f2 is None)
                assert (x1 is not None or x2 is not None)
                assert (x1 is None or x2 is None)

            if f1 is None:
                f1 = self.f(x1)
            if f2 is None:
                f2 = self.f(x2)

            if f1 <= f2:
                # x2 = x1
                # f2 = f1
                a0, b0, x2, x1, f2, f1 = a0, x2, x1, None, f1, None
            else:
                # x1 = x2
                # f1 = f2
                a0, b0, x1, x2, f1, f2 = x1, b0, x2, None, f2, None

        assert x1 is not None or x2 is not None

        if x1 is not None:
            self.result = x1
        else:
            self.result = x2


#
# Functions to optimize
#

def f1(x):
    return x ** 3


def f2(x):
    return abs(x - 0.2)


def f3(x):
    return x * math.sin(1 / x)


def main(epsilon):
    #
    # Exhaustive Search
    #
    for func in (OneDimExhaustiveSearch(0.0, 1.0, epsilon, f1),
                 OneDimExhaustiveSearch(0.0, 1.0, epsilon, f2),
                 OneDimExhaustiveSearch(0.01, 1.0, epsilon, f3)):
        func.optimize()
        logging.info(
            "OneDimExhaustiveSearch -> result=%s - eval_count=%s",
            func.result, func.eval_count)

    #
    # Dichotomy
    #
    DELTA = 0.00009
    for func in (OneDimDichotomy(0.0, 1.0, epsilon, DELTA, f1),
                 OneDimDichotomy(0.0, 1.0, epsilon, DELTA, f2),
                 OneDimDichotomy(0.01, 1.0, epsilon, DELTA, f3)):
        func.optimize()
        logging.info(
            "OneDimDichotomy -> result=%s - eval_count=%s - iter_count=%s",
            func.result, func.eval_count, func.iter_count)

    #
    # Golden Section
    #
    for func in (OneDimGoldenSection(0.0, 1.0, epsilon, f1),
                 OneDimGoldenSection(0.0, 1.0, epsilon, f2),
                 OneDimGoldenSection(0.01, 1.0, epsilon, f3)):
        func.optimize()
        logging.info(
            "OneDimGoldenSection -> result=%s - eval_count=%s - iter_count=%s",
            func.result, func.eval_count, func.iter_count)


def setup_logging(level=logging.DEBUG):
    logging.basicConfig(level=level, stream=sys.stdout)
    logging.getLogger('matplotlib').setLevel(logging.WARNING)


if __name__ == '__main__':
    setup_logging()
    main(epsilon=0.001)
    logging.info("Finished OK")
